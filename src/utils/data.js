import { AiFillHtml5 } from "react-icons/ai";
import { DiCss3 } from "react-icons/di";
import { IoLogoJavascript } from "react-icons/io";
import {RiReactjsLine} from "react-icons/ri";
import {GrNode} from "react-icons/gr";
import {SiExpress} from "react-icons/si";
import {DiMongodb} from 'react-icons/di';
import {SiRedux} from 'react-icons/si';
import {AiFillGitlab} from 'react-icons/ai';
import Seo from '../../public/seo.png'

// MY PROJECT 
import project1 from '../../public/Coinbase.png';
import project2 from '../../public/huddle1.png';
import project3 from '../../public/food.png';
import project4 from '../../public/andress.png';
import project5 from '../../public/inventory.png';
import project6 from '../../public/tesla.png';
import project7 from '../../public/blog.png';
import project8 from '../../public/quiz.png';
import project9 from '../../public/real-state.png';
import project10 from '../../public/travel.png';
import project11 from '../../public/Amazon.png'

// MY CERTIFICATE 
import zuit1 from '../../public/zuit1.jpeg';
import zuit2 from '../../public/zuit2.jpg';
import zuit3 from '../../public/zuit3.jpg';
import zuit4 from '../../public/zuit43.jpg';
import tesda1 from '../../public/Tesda.jpg'
import tesda2 from '../../public/tesda-2.jpg'
import mern from '../../public/mern-udemy.jpg'

// MY MOMENTS
import pic1 from '../../public/pic1.jpg'
import pic2 from '../../public/pic2.jpg'
import pic3 from '../../public/pic3.jpg'
import pic4 from '../../public/pic4.jpg'
import pic5 from '../../public/pic5.jpg'
import pic6 from '../../public/pic6.jpg'
import pic7 from '../../public/pic7.jpg'
import pic8 from '../../public/pic8.jpg'
import pic9 from '../../public/pic9.jpg'
import pic10 from '../../public/pic10.jpg'
import pic11 from '../../public/pic11.jpg'
export const projectExperience = [
  {
    name: "HTML",
    paragraph: "I have a basic familiarity with HTML, which stands for HyperText Markup Language. HTML is used to structure and present content on websites, enabling the creation of web pages with various elements like text, images, links, and more.",
    icon: AiFillHtml5,
    bg: "#D3D3D3",
    color: "#e34c26"
  },
  {
    name: "CSS",
    paragraph:"I possess fundamental knowledge of CSS, or Cascading Style Sheets. CSS is a crucial web technology that enhances the appearance and layout of HTML elements on websites. It's used to apply styles, such as colors, fonts, spacing, and positioning, making web pages visually appealing and user-friendly.",
    icon: DiCss3,
    bg: "#D3D3D3",
    color:"blue"
    
  },

  {
    name: "JavaScript",
    paragraph: "I possess a basic understanding of JavaScript (JS), a versatile programming language used mainly for crafting interactive and dynamic website elements. Leveraging concepts from both functional programming and object-oriented programming (OOP), JS empowers developers to create user interactions, animations, real-time updates, and more. This integration of paradigms enhances the overall user experience while offering flexibility in coding approaches.",
    icon: IoLogoJavascript,
    bg: "#323330",
    color:"#f0db4f"

  },
  {
    name: "React.js",
    paragraph:"My familiarity with React.js is foundational. React.js is a popular JavaScript library used for building user interfaces, particularly for web applications. It employs a component-based approach, allowing developers to create reusable UI components that update efficiently based on changes in data. This facilitates the development of interactive and responsive interfaces, contributing to a smoother and more engaging user experience.",
    icon: RiReactjsLine,
    bg: "#323330",
    color:"#7cc5d9"

  },
  {
    name: "Node.js",
    paragraph: "My understanding of Node.js is at a basic level. Node.js is a runtime environment that enables server-side execution of JavaScript. It's built on the V8 JavaScript engine and allows developers to create server applications using JavaScript. Node.js is known for its non-blocking, event-driven architecture, which makes it suitable for handling asynchronous tasks and building scalable, real-time applications.",
    icon: GrNode,
    bg: "#303030",
    color:"#68a063"
  },
  {
    name: "Express.js",
    paragraph: "My familiarity with Express.js is foundational. Express.js is a web application framework for Node.js, designed to simplify the process of building web applications and APIs. It provides tools and features to handle routing, middleware, and other aspects of web development. By streamlining the creation of server-side applications, Express.js facilitates efficient development and deployment of web services.",
    icon: SiExpress,
    bg: "#303030",
    color:"white"
  },
  {
    name: "MonggoDB",
    paragraph: "I have a basic understanding of MongoDB. MongoDB is a popular NoSQL database that uses a document-oriented approach to store data. It's designed to handle unstructured or semi-structured data and allows for flexible and scalable data storage. MongoDB's JSON-like documents make it easy to work with various data types, and its ability to distribute data across multiple servers enhances performance and availability for applications",
    icon: DiMongodb,
    bg: "#303030",
    color:"green"
  },
  {
    name: "Redux",
    paragraph: "My comprehension of Redux is foundational. Redux is a state management library for JavaScript applications, often used with frameworks like React. It provides a predictable and centralized way to manage application state, making it easier to track and update data across components. Redux follows a unidirectional data flow and employs concepts like actions, reducers, and a single store to maintain a clear and manageable application state, which enhances code organi",
    icon: SiRedux,
    bg: "#303030",
    color:"#764abc"
  },
  {
    name: "GitLab",
  paragraph: "I possess a basic understanding of GitLab. GitLab is a web-based platform that provides tools for version control, source code management, and CI/CD (Continuous Integration/Continuous Deployment). Similar to GitHub, GitLab offers features for collaboration, issue tracking, and code review, making it a comprehensive solution for software development projects. GitLab can be self-hosted or used through their cloud service, offering developers a centralized hub for managing their codebase and development workflow.",
    icon: AiFillGitlab,
    bg: "#303030",
    color:"#e24329"
  },

];

export const WhatDoIHelp = [
  "On-Page SEO: Optimize content and elements on web pages. Off-Page SEO: Build external factors like links and reputation.Technical SEO: Enhance website's technical aspects for search engines.",
  "We use process design to create digital products. Besides that also help their business",
];

export const workExp = [
  {
    place: "OSOlink Solutions ",
    tenure: "March 2023 - August 2023",
    role: "Jr Web Developer",
    detail:
      "Contributed to team sucess by optimizibg online presence through SEO strategies and content  management on wordpress, key role in enhancement search engine visibility and user engagement by updating and optimizing website content",
  },
  {
    place: "Software Development Bootcamp - Zuitt Inc.",
    tenure: "July 2022 - August 2022",
    role: "Bootcamp Traning",
    detail:
      "Intensive MERN stack bootcamp gradauate. Hands-on experience building full-stack web apps from scratch. Understanding in MonggoDB, Express.js Node.js and React. Collaborative projects enhanced teamwork and problem-solving skills",
  },
  {
    place: "TESDA- Technical Education and Skill ",
    tenure: "Oct 2021 - December 2021",
    role: "Traning program in Web Development NC3",
    detail:
      "Conmpleted TESDA's comprehensive web development program, HTML,CSS , JavaScript, and essential tools, Designing dynamic and user-freindly websites. Ready to contribute effective to modern web development projects .",
  },
];


export const projectsFrontEnd = [
  {
    title: "Travel App",
    img:  project10,
    gitlab:"https://gitlab.com/zuitt-196/trave-app-react-js/-/commit/bb368e4e41b1858a59cd3f8c00ab339621cd5415",
    live:"https://trave-app-react-js.vercel.app/"
  },
  {
    title:"Coinbase App",
    img:project1,
    gitlab:"https://gitlab.com/zuitt-196/coinbase-react-app",
    live:"https://coinbase-react-app.vercel.app/"
  },
  {
    title: "Real-state App",
    img:project9,
    gitlab:"https://gitlab.com/zuitt-196/real-state-react/-/commit/3f00d83803aee06cb6fc19174143cf34f0256d22",
    live:"https://real-state-react-theta.vercel.app/"
  },
  {
    title: "Tesla clone App",
    img: project6,
    gitlab:"https://gitlab.com/zuitt-196/tesla-clone-website/-/commit/c4c722026b27a5f6d4c91321e93988c81bc8f5a8",
    live:"https://tesla-clone-website-nu.vercel.app/"
  },
  {
    title: "Andress School blog",
    img:project4,
    gitlab:"https://github.com/vhongbercasio/Front-End-project-1.io",
    live:"https://luminous-puffpuff-5432b4.netlify.app/"
  },
  {
    title: "Huddle App",
    img:project2,
    gitlab:"https://gitlab.com/zuitt-196/react-page-styling",
    live: "https://react-page-styling-zudq.vercel.app/"
  },
  {
    title: "Food App",
    img: project3,
    gitlab:"https://gitlab.com/zuitt-196/food-app-react-js",
    live:"https://food-app-react-js-ln4d.vercel.app/",
  },
  {
    title: "Amazon Store App",
    img: project11,
    gitlab:"https://gitlab.com/zuitt-196/amazon-ecommerce",
    live:"https://amazon-ecommerce-theta.vercel.app/",
  }
]

export const projectFullStack = [
  {
    title:"Inventory",
    img:project5,
    gitlab: "https://gitlab.com/zuitt-196/siltoninvent-app-mern",
    

  },
  {
    title: "Quiz App",
    img:project8,
    gitlab:"",
 
  },
  {
    title:"Blog application",
    img:project7,
    gitlab:"https://gitlab.com/zuitt-196/mern-app-blog/-/tree/master/Blog-app?ref_type=heads",

  }
]
export const comments = [
  {
    name: "My Computer Set Up",
    post: "Creative Manager",
    comment:
      "As a working student, I diligently earn money to fulfill my aspiration of acquiring a computer, an invaluable tool that opens the door to learning coding and programming. Despite the challenges posed by the conflicting demands of my job and studies, I remain resolutely positive. This computer represents more than just a device; it embodies my determination to enhance my skills and carve a path towards a promising future. Balancing work, education, and self-improvement might be arduous, yet the prospect of mastering code keeps me motivated and optimistic about the journey ahead.",
    img: pic1,
  },
  {
    name: "Zuitt Bootcamp Training",
    post: "Creative Manager",
    comment:
      "Enrolling in the coding bootcamp at Zuitt has been a transformative experience on my journey to learn code. Stepping into the world of programming through their comprehensive training program, I've been exposed to a diverse array of coding languages, tools, and real-world projects. The immersive nature of the bootcamp, coupled with the guidance of experienced mentors, has accelerated my understanding of coding concepts and their practical applications. Despite the challenges and late-night coding sessions, every breakthrough I achieve reinforces my determination to succeed in this dynamic field. With each line of code written and each problem solved, I'm gaining the skills and confidence I need to turn my passion for coding into a fulfilling and impactful career.",
    img: pic2,
  },
  {
    name: "Graduation Day as BSIT",
    post: "Creative Manager",
    comment:
      "Graduating alongside my friends, despite not excelling academically or possessing exceptional talents, has been a testament to our collective determination. Our bond has grown stronger as we pursued success together. Despite our diverse backgrounds, we've supported each other unconditionally. In the face of challenges, we've shared advice and offered guidance, navigating the complexities of being working students. Their unwavering presence and insights have illuminated my path, reminding me that success is not solely defined by innate abilities but also by the commitment and support we provide to one another",
    img:pic9,
  },
  {
    name: "TESDA NC3 Web Development ",
    post: "Creative Manager",
    comment:
      "Enrolling in TESDA's training program was a turning point in enhancing my skills. Completing the program and earning the certificate showcased my commitment to growth. The practical knowledge I've gained equips me to excel in my field, armed with confidence and expertise from TESDA's training.",
    img: pic8,
  },
  {
    name: "Delivery Hollow Black",
    post: "Creative Manager",
    comment:
      "Navigating the dual responsibilities of a working student and occasionally being immersed in construction tasks, such as the precise arrangement of hallow blocks or ensuring their timely delivery to fulfill customer orders,",
    img: pic6,
  },
  {
    name: "My Motorcylce",
    post: "Creative Manager",
    comment:
      "Receiving a motorcycle from my boss as a working student has proven to be an invaluable asset that extends beyond transportation. This generous gesture not only ensures my smooth commute to school but also emphasizes my boss's unwavering support for my education and professional journey.",
    img: pic10,
  },
  {
    name: "Outing with worker in the beach",
    post: "Creative Manager",
    comment:
      "Occasionally, our team of workers enjoys outings initiated by our boss, underscoring their dedication to fostering a well-connected and motivated workforce. These excursions extend beyond our labor roles, allowing us to bond in a more relaxed setting.",
    img:pic11,
  },
  {
    name: "Cooking time",
    post: "Creative Manager",
    comment:
      "As a working student, I am also the dedicated household cook from morning until night, if no class",
    img: pic5,
  },
  {
    name: "Graduated as BSIT ",
    post: "Creative Manager",
    comment:
      "I take immense pride in my achievement of graduating with a degree in BSIT while managing the responsibilities of a working student. This journey exemplified my dedication, perseverance, and ability to balance academic pursuits with real-world experience. The knowledge gained in both realms has provided me with a unique skill set, combining theoretical expertise with practical insights. This accomplishment marks not only the culmination of my educational endeavors but also a testament to my determination to excel in the face of challenges.",
    img: pic3,
  },
  {
    name: "TESDA NC2 and NC3 ",
    post: "Creative Manager",
    comment:
      "Armed with both NC2 and NC3 certifications from Caraga, I am poised to confidently embark on my job search. These qualifications represent a substantial investment of time and effort into acquiring specialized skills that are in demand within the industry. As I prepare to enter the job market, I am equipped with the expertise necessary to excel in roles that require a deep understanding of the field. These certifications not only bolster my resume but also underscore my commitment to professional development and my readiness to contribute effectively to any prospective employer's team..",
    img: pic7,
  },
  
  
 
];

export const certificatesTraining = [
  {
    title: "Zuitt Award Completion",
    img:zuit1,

  },
  {
    title: "Zuitt Back-End Course",
    img:zuit2,
  },
  {
    title: "Zuitt Front-End Course",
    img:zuit3,
  },
  {
    title: "Zuitt Full-stack Course",
    img:zuit4,
  },

  {
    title: "TESDA NC3 Web Development",
    img:tesda1,
  },
  {
    title: "TESDA NC2 CSS",
    img:tesda2,
  },
  {
    title: "Udemy MERN Stack",
    img:mern,
  },


]

export const sliderSettings = {
  dots: true, 
  infinite: false,
  speed: 1000,
  slidesToShow: 3,
  slidesToScroll: 1,
  initialSlide: 0,
  touchMove: true,
  useCSS: true,

  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

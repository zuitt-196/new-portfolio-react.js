import React from 'react'
import { workExp } from '../../utils/data'
import css from './WorkExperienceStyle.module.scss'


const WorkExperience = () => {
  return (
    <section className={`paddings ${css.wrapper}`}>
        <a className="anchor" id="experiences"></a>
        <div className={`flexCenter innerWidth ${css.container}`} >
            <span className="primaryText yPaddings">My Work Expereince and Tranings</span>
            
            <div className={`flexCenter ${css.experience}`}>
                {
                    workExp?.map((exp, i) =>(
                            <div className={`flexCenter ${css.exp}`} key={i}>
    
                                {/* POST */}
                                <div className={css.post}>
                                    <h1>{exp.place}</h1>
                                    <p>{exp.tenure}</p>
                                </div>

                                {/* ROLE */}
                                <div className={css.role}>
                                    <h1>{exp.role}</h1>
                                    <p>{exp.detail}</p>
                                </div>
                            </div>     
                        ))}

                        {/* PROGRESS SECTION */}
                <div className={css.progressbar}>
                    <div className={css.line}></div>
                            <div><div className={css.circle} style={{background: "#286F6C"}}></div></div>
                            <div><div className={css.circle} style={{background: "#F2704E"}}></div></div>
                            <div><div className={css.circle} style={{background: "#EEC048"}}></div></div>
                 </div>
            </div>
        </div>

    </section>
  )
}

export default WorkExperience
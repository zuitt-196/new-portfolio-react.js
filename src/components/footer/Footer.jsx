import React from 'react';
import css from './FooterStyle.module.scss'

const Footer = () => {
  return (
    <div className={css.wrapper}>
        
        <div className={ `innerWidth  yPaddings flexCenter  ${css.container}`}>
            
            {/* LEFT SECTION */}
            <div className={css.left}>
                <span className="primaryText">
                    Let's make something <br /> amazing together
                </span>
                <span className="primaryText">
                    start by <a href="mailto:berccaiobonbong@gmail.com">saying hi and email me</a>
                </span>
            </div>

            {/* RIGHT SECTION */}
            <div className={css.right}>
                
                <div className={css.info}>
                    <span className='secondaryText'>
                        Information 
                    </span>
                    <p>Puan, Carmelo Homes Davao city</p>
                </div>
                <div className={css.menu}>
                    <li>About</li>
                    <li>Experiences</li>
                    <li>Certificate</li>
                    <li>Skills</li>
                    <li>Projects</li>
                    <li>Blog</li>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Footer

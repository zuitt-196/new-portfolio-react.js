import React from 'react'
import { certificatesTraining } from '../../utils/data'
import css from './CertifacteStyle.module.scss'
import {FaAward} from 'react-icons/fa'
const Certifacte = () => {
  return (
    <div className={`paddings ${css.wrapper}`}>
        <a href="" className="anchor" id="certificate"></a>
            <div className={`innerWidth  flexCenter ${css.container}`}>  

            <div className={`flexCenter topPaddings ${css.heading}`}>
              <div>
                    <span className="primaryText">
                          My Certificate Of Training <FaAward color="gold"/> <FaAward color="gold"/><FaAward color="gold"/>
                    </span>
                    <p style={{marginTop: "30px"}}>
                        Eager to learn and achieve as developer, I'm committed to undergoing training to enhance my skills and make my goals a reality.
                    </p>
                  
                </div>
        </div>
            <div className={`flexCenter ${css.showCertificates}`}>
                { certificatesTraining?.map((certificate, i) => (
                    <div key={i} >
                        <img src={certificate.img} alt="img" />
                        <div>
                            <span className='secondaryText'>{certificate.title}</span>
                        </div>
                    </div>
                ))}
            </div>

            </div>
    </div>
  )
}

export default Certifacte

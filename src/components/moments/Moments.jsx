import React from 'react'
import css from './MomentStyle.module.scss'
import Slider from 'react-slick'
import { comments, sliderSettings } from '../../utils/data';
import { FaHandPointRight } from 'react-icons/fa';

const Moments = () => {
  return (
    <section className={`yPaddings ${css.wrapper}`}> 
    <a href="" className="anchor" id="blog"></a>
      <div className={`yPaddings innerWidth ${css.container}`}>
          
          <div className={`flexCenter ${css.heading}`}>
            <span className='primaryText'>My Blog</span>
            <p style={{marginTop: '2rem'}} className='secondaryText'>As a working student,I diligently pursue my dream, efficiently balacing studies, work, and coding learning</p>
            <p style={{fontStyle: "italic"}} className="secondaryText">"Balancing the itricate threads of studies, work, and the the pursuit of coding proficiency ,I weave the fabric of my aspirations as a diligent working student "</p> 
          </div>
          <div className={`yPaddings ${css.sliderComments}`}>
            <span className='secondaryText'>Slide the image if you want to Knowing my life </span> 
            <FaHandPointRight size={50} color="blue" className={css.iconArrow}/>
          </div>
    


{/* CAROUCEL  */}
<div className={`yPaddings ${css.comment}`}>
    <Slider
      {...sliderSettings}
      className={css.slider}
    >
        {comments.map((comment,i) =>(
            <div key={i} className={`flexCenter ${css.comments}`}>
                    <img src={comment.img} alt="img"/>
                    <span className='secondaryText'>{comment.name}</span>
               

                    <div className={css.bio}>
                        <span>{comment.comment}</span>
              
                    </div>
            </div>
        ))}
    </Slider>
</div>
</div>  

    </section>
  )
}

export default Moments

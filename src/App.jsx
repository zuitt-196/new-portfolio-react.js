import { FaCertificate } from "react-icons/fa";
import Certifacte from "./components/certificate/Certifacte";
import Experties from "./components/Expertise/Experties";
import Footer from "./components/footer/Footer";
import Header from "./components/header/Header";
import Hero from "./components/hero/Hero";
import Moments from "./components/moments/Moments";
import Portfolio from "./components/portfolio/Portfolio";
import WorkExperience from "./components/work/WorkExperience";

// PARENT STYLE MODULE SCSS
import css from './styles/app.module.scss'
const App = () => {
  return (
      <div className={`bg-primary ${css.container}`}>
          <Header/>
          <Hero/>
          <WorkExperience/>
          <Certifacte/>
          <Experties/>
          <Portfolio/>
          <Moments/>
          <Footer/>
        
      </div>
  )
};

export default App;
